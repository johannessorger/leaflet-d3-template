/**
 * Created by Johannes Sorger on 16.01.18.
 */


var mymap;
var svg;
var g;
var bounds;
var padding = 200;
var pointPositions = [];
var circles;
var paths;
var scaleSVG = true;
var crad = 300;

//test/demo data
var data = [
    {key: 0, coords: [48.205, 16.375]}, //vienna
    {key: 1, coords: [46.610, 13.864]},
    {key: 2, coords: [40.712, -74.005]} //new york
];

//used for updating the projection of the geojson path when zooming or changing the view
function projectPoint(x, y) {
    var point = mymap.latLngToLayerPoint(new L.LatLng(y, x));
    this.stream.point(point.x, point.y);
}
var transform = d3.geoTransform({point: projectPoint}),
    path = d3.geoPath().projection(transform);


function onLoadPage()
{
    initMap();
    loadTestData(data);
    loadGeoJson("data/ALL.geojson");
}


function initMap()
{
    //map is centered on austria
    mymap = L.map('mapid').setView([47.488,12.881], 7);

    //mapbox tiles need an access token (retrievable via free mapbox account)
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 12,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiamFyb25pbW9lIiwiYSI6ImNqY2hwcHFpcjJhMHEycXBlbml1bmU0ZDgifQ.IkyxLIqX3kBME-RfMBPXJg'
    }).addTo(mymap);

    mymap.on('viewreset', updateView);
    mymap.on('zoom', updateView);

    //this svg holds the d3 visualizations
    svg = d3.select(mymap.getPanes().overlayPane).append("svg");
    g = svg.append("g").attr("class", "leaflet-zoom-hide");
}


function loadTestData(indata)
{
    //create lat/lng coords for each data item
    indata.forEach(createLatLng);

    circles = g.selectAll("circle")
        .data(indata)
        .enter()
        .append("circle")
        .attr("r", crad);

    updateView();
}


//assigns each input object a new LatLng variable based on a given x/y coordinate
//TODO: adapt to input data in terms of coordinate access
function createLatLng(d)
{
    d.LatLng = new L.LatLng(d.coords[0], d.coords[1]);
}


//calculate the projection of gis coordinates to the leaflet map layer (canvas coordinates)
function updatePosition(d)
{
    var newpoint = mymap.latLngToLayerPoint(d.LatLng);
    pointPositions.push(newpoint);
}


//triggered when zooming: projection to map and bounds need to be updated
function updateView()
{
    //clear old positions
    pointPositions = [];
    data.forEach(updatePosition);

    if(paths != undefined)    paths.attr("d", path);

    circles.attr("cx",function(d) { return mymap.latLngToLayerPoint(d.LatLng).x});
    circles.attr("cy",function(d) { return mymap.latLngToLayerPoint(d.LatLng).y});
    if(scaleSVG) circles.attr("r",function(d) { return crad/1400*Math.pow(2,mymap.getZoom())});


    bounds = calculateDataBounds(pointPositions);
    var topLeft = bounds[0];
    var bottomRight = bounds[1];

    //alternatively, the path bounds could be calculated
    //     var gbounds = path.bounds(collection),
    //         topLeft = gbounds[0],
    //         bottomRight = gbounds[1];

    svg .attr("width", bottomRight.x - topLeft.x + 2*padding)
        .attr("height", bottomRight.y- topLeft.y + 2*padding)
        .style("left", topLeft.x-padding + "px")
        .style("top", topLeft.y-padding + "px");

    g .attr("transform", "translate(" + (-topLeft.x+padding) + ","
        + (-topLeft.y+padding) + ")");
}


//calculate top left and bottom right extents of given features/shapes
function calculateDataBounds(features)
{
    var minx = 0, miny = 0, maxx = 0, maxy = 0;

    //find maxima
    for(var i=0; i<features.length; i++)
    {
        if(features[i].x > maxx) maxx = features[i].x;
        if(features[i].y > maxy) maxy = features[i].y;
    }

    minx = maxx;
    miny = maxy;

    //find minima
    for(var i=0; i<features.length; i++)
    {
        if(features[i].x < minx) minx = features[i].x;
        if(features[i].y < miny) miny = features[i].y;
    }

    var topleft = {};
    topleft.x = minx;
    topleft.y = miny;

    var bottomright = {};
    bottomright.x = maxx;
    bottomright.y = maxy;

    var bounds = [];
    bounds[0] = topleft;
    bounds[1] = bottomright;

    return bounds;
}


function loadGeoJson(jsonpath)
{
    d3.json(jsonpath, function(error, collection) {
        if (error) throw error;

        paths = g.selectAll("path")
            .data(collection.features)
            .enter().append("path")
            .attr("fill", "white")
            .attr("opacity", 0.5);

        paths.attr("d", path);

    });
}

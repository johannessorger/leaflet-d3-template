/**
 * Created by hannes on 26.01.18.
 */

var oldOrigin;
var oldCenter;
var debugPoints;
var oldX;
var oldY;

var mymap;
var data = [
    {key: 0, coords: [48.205, 16.375]},
    {key: 1, coords: [46.610, 13.864]}
];


function onLoadPage()
{
    initMap();

    data.forEach(createLatLng);

    initGFX();
}

var camera;
var geometry;
var renderer;
var scene;

function initGFX()
{
    scene = new THREE.Scene();
    // var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    // camera.position.set(0, 0, 100);
    // camera.lookAt(new THREE.Vector3(0, 0, 0));

    camera = new THREE.OrthographicCamera(window.innerHeight , -window.innerHeight , window.innerWidth , -window.innerWidth , 1, 1000 );
    camera.position.set(0, 0, 1000);
    // camera.lookAt(new THREE.Vector3(0, 0, -1));
    scene.add( camera );

    renderer = new THREE.WebGLRenderer({ alpha: true });
    //TODO: assumes that the map is rendered in full screen - if it is not: adapt to map extents
    renderer.setSize( window.innerWidth, window.innerHeight );
    mymap.getPanes().overlayPane.appendChild( renderer.domElement );

    //demo cube
    // var geom = new THREE.BoxGeometry( 1, 1, 1 );
    // var mater = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    // var cube = new THREE.Mesh( geom, mater );

//    scene.add( cube );
   camera.position.z = 5;
//    demo line

    var material = new THREE.LineBasicMaterial({ color: 0x00ff00 });

    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(-100000, 0, 0));
    geometry.vertices.push(new THREE.Vector3(100000, 0, 0));
    var lineX = new THREE.Line(geometry, material);
    scene.add(lineX);

    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(0, -100000, 0));
    geometry.vertices.push(new THREE.Vector3(0, 100000, 0));
    var lineY = new THREE.Line(geometry, material);
    scene.add(lineY);


    oldX = projectVector(scene.children[1].position, camera, renderer);
    oldY = projectVector(scene.children[2].position, camera, renderer);

    // geometry = new THREE.Geometry();
    // geometry.vertices.push(new THREE.Vector3(-10, 0, 0));
    // geometry.vertices.push(new THREE.Vector3(0, 10, 0));
    // geometry.vertices.push(new THREE.Vector3(10, 0, 0));
    // var line = new THREE.Line(geometry, material);
    // scene.add(line);


    // lineX.translateX(-oldOrigin.x);
    // lineX.translateY(-oldOrigin.y);
    //
    // lineX.translateX(data[0].layerpoint.x);
    // lineX.translateY(data[0].layerpoint.y);

    function animate() {
        requestAnimationFrame( animate );

        // cube.rotation.x += 0.01;
        // cube.rotation.y += 0.01;
        //
        // line.rotation.x += 0.01;
        // line.rotation.y += 0.01;

        // line.translateZ(0.1);

        renderer.render( scene, camera );
    }
    animate();
}


function initMap()
{
    //map is centered on austria
    mymap = L.map('mapid').setView([47.488,12.881], 7);

    //mapbox tiles need an access token (retrievable via free mapbox account)
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 12,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiamFyb25pbW9lIiwiYSI6ImNqY2hwcHFpcjJhMHEycXBlbml1bmU0ZDgifQ.IkyxLIqX3kBME-RfMBPXJg'
    }).addTo(mymap);

    mymap.on('viewreset', updateView);
    mymap.on('zoom', updateView);
    mymap.on('move', updateView);


    oldOrigin = mymap.getPixelOrigin();
    oldCenter = getPixelCenter(mymap);

    //
    // //this svg holds the d3 visualizations
    // svg = d3.select(mymap.getPanes().overlayPane).append("svg");
    // g = svg.append("g").attr("class", "leaflet-zoom-hide");

    //     var glLayer = L.canvasOverlay()
    //         .drawing(drawingOnCanvas)
    //         .addTo(leafletMap);
    //     var canvas = glLayer.canvas();
    //
    //     glLayer.canvas.width = canvas.clientWidth;
    //     glLayer.canvas.height = canvas.clientHeight;
}

//returns center in respect to map's pixel origin
function getPixelCenter(map)
{
    var latlngcenter = map.getCenter();
    var pixcenter = map.latLngToLayerPoint(latlngcenter);
    return pixcenter;
}


function createLatLng(d)
{
    d.LatLng = new L.LatLng(d.coords[0], d.coords[1]);
    d.layerpoint = mymap.latLngToLayerPoint(d.LatLng);
}


function updateView()
{
    var newCenter = getPixelCenter(mymap);
    var xDif = oldCenter.x - newCenter.x;
    var yDif = oldCenter.y - newCenter.y;
    // oldCenter = newCenter;
    console.log("center moved by: " + xDif + ", " + yDif);

    var newOrigin = mymap.getPixelOrigin();
    var xOriginDif = oldOrigin.x - newOrigin.x;
    var yOriginDif = oldOrigin.y - newOrigin.y;
    // oldOrigin = newOrigin;
    console.log("origin moved by: " + xOriginDif + ", " + yOriginDif);


    //move projected object vectors by difference
    var newPosX = new THREE.Vector3();
    newPosX.x = oldX.x + xDif;
    newPosX.y = oldX.y + yDif;
    var newPosY = new THREE.Vector3();
    newPosY.x = oldY.x + xDif;
    newPosY.y = oldY.y + yDif;

    //unproject
    var revX = reverseProjection(newPosX, camera, renderer);
    var revY = reverseProjection(newPosY, camera, renderer);

    //setpositions
    scene.children[1].position = revX;
    scene.children[2].position = revY;

    // var proj = toScreenPosition(scene.children[1], camera);
    // console.log(proj);
    //
    // projX = projectVector(scene.children[1].position, camera, renderer);
    // projY = projectVector(scene.children[2].position, camera, renderer);
    // console.log(proj);
}

function toScreenPosition(obj, camera)
{
    var vector = new THREE.Vector3();

    var widthHalf = 0.5*renderer.context.canvas.width;
    var heightHalf = 0.5*renderer.context.canvas.height;

    obj.updateMatrixWorld();
    vector.setFromMatrixPosition(obj.matrixWorld);
    vector.project(camera);

    vector.x = ( vector.x * widthHalf ) + widthHalf;
    vector.y = - ( vector.y * heightHalf ) + heightHalf;

    return {
        x: vector.x,
        y: vector.y
    };

}


function projectVector(pos, camera, renderer) {

    var vector = pos.project(camera);

    vector.x = (vector.x + 1) / 2 * renderer.context.canvas.width;
    vector.y = -(vector.y - 1) / 2 * renderer.context.canvas.height;

    return vector;
}


function reverseProjection(pos, camera, renderer) {

    var vect = new THREE.Vector3();

    vect.x = (2*pos.x / renderer.context.canvas.width) - 1;
    vect.y = (-2*pos.y / renderer.context.canvas.height) + 1;


    var vector = vect.unproject(camera);


    return vector;
}
























